/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.cursojava.students.rs.dao;

import java.util.ArrayList;
import java.util.List;
import py.com.cursojava.students.rs.dto.Alumno;

/**
 *
 * @author Gino Junchaya
 */
public class AlumnoDao implements Dao<Alumno, Integer>{
    
    private final List<Alumno> alumnos = new ArrayList<>();

    @Override
    public List<Alumno> listar() {
        return alumnos;
    }

    @Override
    public Alumno listarPorId(Integer id) {
        for(Alumno alumno : alumnos){
            if(alumno.getId().equals(id)){
                return alumno;
            }
        }
        return null;
    }

    @Override
    public Alumno agregar(Alumno entity) {
        entity.setId(generarIdentificador());
        alumnos.add(entity);
        return entity;
    }

    @Override
    public Alumno editar(Alumno entity) {
        Alumno alumno = listarPorId(entity.getId());
        if(alumno == null){
            throw new RuntimeException("El alumno a actualizar no existe");
        }
        alumno.setAge(entity.getAge());
        alumno.setFirstName(entity.getFirstName());
        alumno.setLastName(entity.getLastName());
        alumno.setNick(entity.getNick());
        return alumno;
    }

    @Override
    public void eliminar(Integer id) {
        Alumno alumno = listarPorId(id);
        if(alumno == null){
            throw new RuntimeException("El alumno a eliminar no existe");
        }        
        alumnos.remove(alumno);
    }
    
    private Integer generarIdentificador(){
        if(alumnos.isEmpty()){
            return 1;
        }
        Integer idMayor = 1;
        for(Alumno alumno : alumnos){
            if(alumno.getId() > idMayor){
                idMayor = alumno.getId();
            }
        }
        return idMayor + 1;
    }
    
}
