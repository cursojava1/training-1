package py.com.cursojava.students.rs.dao;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import py.com.cursojava.students.rs.dto.Alumno;

/**
 *
 * @author Gino Junchaya
 */
@Transactional
@ApplicationScoped
public class JpaAlumnoDao implements Dao<Alumno, Integer>{
    
    @PersistenceContext(unitName = "StudentPU")
    private EntityManager entityManager;
    
    @Override
    public List<Alumno> listar() {
        return entityManager.createQuery("SELECT A FROM Alumno A").getResultList();
    }

    @Override
    public Alumno listarPorId(Integer id) {
        return entityManager.find(Alumno.class, id);
    }

    @Override
    public Alumno agregar(Alumno entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Alumno editar(Alumno entity) {
        return entityManager.merge(entity);
    }

    @Override
    public void eliminar(Integer id) {
        Alumno entity = listarPorId(id);
        if(entity == null){
            throw new RuntimeException("El alumno a eliminar no existe");
        }
        entityManager.remove(entity);
    }
    
}
