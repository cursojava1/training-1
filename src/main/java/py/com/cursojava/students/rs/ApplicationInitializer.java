package py.com.cursojava.students.rs;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Gino Junchaya
 */

@ApplicationPath("/rest")
public class ApplicationInitializer extends Application {
    
//    private final Set<Object> singletons = new HashSet<>();
//    
//    public ApplicationInitializer(){
//        singletons.add(new AlumnoController());
////        singletons.add(new JpaAlumnoDao());
////        singletons.add(new AlumnoService());
//    }
//
//    @Override
//    public Set<Object> getSingletons() {
//        return singletons;
//    }
    
}
