package py.com.cursojava.students.rs.service;

import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import py.com.cursojava.students.rs.dao.JpaAlumnoDao;
import py.com.cursojava.students.rs.dto.Alumno;

/**
 *
 * @author Gino Junchaya
 */
@Named
@RequestScoped
public class AlumnoService {
    
    @Inject
    private JpaAlumnoDao alumnoDao;
    
    public List<Alumno> listar(){
        return alumnoDao.listar();
    }
    
    public Alumno listarPorId(Integer id){
        return alumnoDao.listarPorId(id);
    }
    
    public Alumno agregar(Alumno alumno){
        return alumnoDao.agregar(alumno);
    }
    
    public Alumno editar(Alumno alumno){
        return alumnoDao.editar(alumno);
    }
    
    public void eliminar(Integer id){
        alumnoDao.eliminar(id);
    }
    
    
}
