package py.com.cursojava.students.rs.controller;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import py.com.cursojava.students.rs.dto.Alumno;
import py.com.cursojava.students.rs.service.AlumnoService;

/**
 *
 * @author Gino Junchaya
 */

@ApplicationScoped
@Path("/students")
public class AlumnoController {
    
    @Inject
    private AlumnoService alumnoService;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listar(){
        List<Alumno> alumnos = alumnoService.listar();
        if(alumnos == null || alumnos.isEmpty()){
            return Response.noContent().build();
        }
        return Response.ok(alumnos).build();
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{id}")
    public Response listar(@PathParam("id") Integer id){
        Alumno alumno = alumnoService.listarPorId(id);
        if(alumno == null){
            return Response.status(404).build();
        }
        return Response.ok(alumno).build();
    }    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response agregar(Alumno alumno){
        Alumno guardado = alumnoService.agregar(alumno);
        return Response.status(201).entity(guardado).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editar(Alumno alumno){
        Alumno editado = alumnoService.editar(alumno);
        return Response.ok(editado).build();
    }

    @Path("/{id}")
    @DELETE
    public Response eliminar(@PathParam("id") Integer id){
        alumnoService.eliminar(id);
        return Response.ok().build();
    }
    
}


