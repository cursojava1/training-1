package py.com.cursojava.students.rs.dao;

import java.util.List;

/**
 *
 * @author Gino Junchaya
 * @param <A>
 * @param <B>
 */
public interface Dao<A, B> {
    
    List<A> listar();
    A listarPorId(B id);
    A agregar(A entity);
    A editar(A entity);
    void eliminar(B id);
    
}
