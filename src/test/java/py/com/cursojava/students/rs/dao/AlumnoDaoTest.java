/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.cursojava.students.rs.dao;

import org.junit.Test;
import static org.junit.Assert.*;
import py.com.cursojava.students.rs.dto.Alumno;

/**
 *
 * @author Gino Junchaya
 */
public class AlumnoDaoTest {
    
    @Test
    public void test() {

        
        AlumnoDao alumnoDao = new AlumnoDao();
        
        System.out.println(alumnoDao.listar());
        
        Alumno alumno1 = new Alumno();
        alumno1.setAge(25);
        alumno1.setFirstName("Leandro");
        alumno1.setLastName("Britez");
        alumno1.setNick("lbritez");
        
        Alumno alumno2 = new Alumno();
        alumno2.setAge(21);
        alumno2.setFirstName("César Pablo");
        alumno2.setLastName("Battilana");
        alumno2.setNick("cpb");
        
        alumnoDao.agregar(alumno1);
        alumnoDao.agregar(alumno2);
        
        System.out.println(alumnoDao.listar());
        
        alumno1.setAge(20);
        
        alumnoDao.editar(alumno1);
        
        alumnoDao.eliminar(2);
        
        System.out.println(alumnoDao.listar());        
        
        assertTrue(true);
    }
    
}
